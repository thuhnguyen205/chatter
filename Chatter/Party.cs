﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Chatter
{
    public partial class Party : Form
    {
        private string strName;
        private Port pPort;
        private Thread thReceive;
        
        public Party(string strNm, Port pPortFromChannel) {
            InitializeComponent();

            strName = strNm;
            this.Text = strNm;

            pPort = pPortFromChannel;
            thReceive = null;
        }

        private void Party_Load(object sender, EventArgs e)
        {
            ThreadStart tsFunction = new ThreadStart(vReceiveMsg);
            thReceive = new Thread(tsFunction);
            thReceive.Start();
        }
        private delegate void AppendText(string strNewText);
        private void btnSend_Click(object sender, EventArgs e)
        {
            string strNewMsg = txtNewMsg.Text;
            string strFullMsg = "";
            strFullMsg = strName + ": " + strNewMsg;
            pPort.vSend(strFullMsg);
            rtbConversation.AppendText(strFullMsg +"\r\n");
            txtNewMsg.Clear();
        }
        private void vReceiveMsg()
        {
            for (; ; )
            {
                string strReceiveMsg = pPort.vReceive();
                AppendText NewAppendText = new AppendText(rtbConversation.AppendText);
                this.Invoke(NewAppendText, strReceiveMsg + "\r\n");
            }
        }

        private void Party_FormClosing(object sender, FormClosingEventArgs e)
        {
            thReceive.Abort();
        }

    }
}
