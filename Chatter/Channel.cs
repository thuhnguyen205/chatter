﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Chatter
{
    public class Channel
    {
        private Port pPortA;
        private Port pPortB;

        public Port PortA
        {
            get
            {
                return pPortA;
            }
        }
        public Port PortB
        {
            get
            {
                return pPortB;
            }
        }
     
        public Channel()
        {
            Link lLinkA = new Link();
            Link lLinkB = new Link();
            pPortA = new Port(lLinkA, lLinkB);
            pPortB = new Port(lLinkB, lLinkA);
        }

    }
  
    public class Port
    {
        private Link lLinkSend;
        private Link lLinkReceive;

        public Port(Link lLinkA, Link lLinkB)
        {
            lLinkSend = lLinkA;
            lLinkReceive = lLinkB;
        }
        public void vSend(string strMsg)
        {
            lLinkSend.vEnter(strMsg);
        }
        public string vReceive()
        {
            string strMsg = lLinkReceive.vRetrieve();
            return strMsg;
        }
    }
    public class Link
    {
        private Semaphore semMessageCount;
        private Mutex mutProtectLink;
        private Queue<string> qstrNewMsg;

        public Link()
        {
            semMessageCount = new Semaphore(0, Int32.MaxValue);
            mutProtectLink = new Mutex();
            qstrNewMsg = new Queue<string>();
        }

        public void vEnter(string strNewMsg)
        { 
            mutProtectLink.WaitOne();
            qstrNewMsg.Enqueue(strNewMsg);
            mutProtectLink.ReleaseMutex();
            semMessageCount.Release();
        }
        public string vRetrieve()
        {
            semMessageCount.WaitOne();
            mutProtectLink.WaitOne();
            string strMsg = qstrNewMsg.Dequeue();
            mutProtectLink.ReleaseMutex();
            return strMsg;
        }
    }
}
