﻿namespace Chatter
{
    partial class Party
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.rtbConversation = new System.Windows.Forms.RichTextBox();
            this.txtNewMsg = new System.Windows.Forms.TextBox();
            this.btnSend = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // rtbConversation
            // 
            this.rtbConversation.Location = new System.Drawing.Point(12, 13);
            this.rtbConversation.Name = "rtbConversation";
            this.rtbConversation.ReadOnly = true;
            this.rtbConversation.Size = new System.Drawing.Size(320, 207);
            this.rtbConversation.TabIndex = 2;
            this.rtbConversation.TabStop = false;
            this.rtbConversation.Text = "";
            // 
            // txtNewMsg
            // 
            this.txtNewMsg.Location = new System.Drawing.Point(12, 228);
            this.txtNewMsg.Name = "txtNewMsg";
            this.txtNewMsg.Size = new System.Drawing.Size(275, 20);
            this.txtNewMsg.TabIndex = 0;
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(293, 226);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(40, 23);
            this.btnSend.TabIndex = 1;
            this.btnSend.Text = "Send";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // Party
            // 
            this.AcceptButton = this.btnSend;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(344, 261);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.txtNewMsg);
            this.Controls.Add(this.rtbConversation);
            this.Name = "Party";
            this.Text = "Party";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Party_FormClosing);
            this.Load += new System.EventHandler(this.Party_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtbConversation;
        private System.Windows.Forms.TextBox txtNewMsg;
        private System.Windows.Forms.Button btnSend;
    }
}